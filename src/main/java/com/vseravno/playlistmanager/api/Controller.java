package com.vseravno.playlistmanager.api;

import com.vseravno.playlistmanager.PlaylistManager;
import com.vseravno.playlistmanager.PlaylistObject;
import com.vseravno.playlistmanager.TrackObject;
import com.vseravno.playlistmanager.YAMLConfig;
import com.vseravno.playlistmanager.db.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.*;

@RestController
public class Controller {

  private static final Logger logger = LoggerFactory.getLogger(Controller.class);

  private final AccountService accountService;
  private final PlaylistService playlistService;
  private final TrackService trackService;
  private final YAMLConfig yamlConfig;

  @Autowired
  public Controller(
      AccountService accountService,
      PlaylistService playlistService,
      TrackService trackService,
      YAMLConfig yamlConfig) {
    this.accountService = accountService;
    this.playlistService = playlistService;
    this.trackService = trackService;
    this.yamlConfig = yamlConfig;
  }

  @PostMapping("/auth")
  String auth(@RequestBody AuthRequest body) throws Exception {
    String authCode = body.getAuthCode();
    SpotifyClient client = SpotifyClient.create(AuthorizationCode.auth(authCode, yamlConfig));
    UserAuth userAuth = client.getUserAuth();
    // for a new user
    if (!accountService.userExist(userAuth)) {
      accountService.createAccount(userAuth);
      // for returning user
    } else {
      accountService.updateTokens(userAuth);
    }
    return userAuth.getAccessToken();
  }

  // send all user playlists to frontend
  @GetMapping("/allplaylists")
  List<PlaylistObject> allplaylists(
      @RequestHeader("Token") String accessToken, HttpServletResponse response) throws Exception {
    Account currentUser = accountService.getUserByAccessToken(accessToken);
    SpotifyClient client = SpotifyClient.create(currentUser.getRefreshToken(), yamlConfig);
    UserAuth userAuth = client.getUserAuth();
    accountService.updateTokens(userAuth);
    PlaylistManager manager = new PlaylistManager(client);
    response.setHeader("Token", userAuth.getAccessToken());
    // if user has no target playlist, treat him like a new user
    if (!client.isUserActive(accountService.getTargetPlaylistSid(userAuth))) {
      return manager.getUserPlaylists();
    } else {
      List<Playlist> dbPlaylists = playlistService.getUserPlaylists(userAuth);
      return manager.getUserPlaylists(dbPlaylists);
    }
  }

  @PostMapping("/chosenplaylists")
  void chosenPlaylists(
      @RequestHeader("Token") String accessToken, @RequestBody List<PlaylistObject> playlists)
      throws Exception {
    Account currentUser = accountService.getUserByAccessToken(accessToken);
    SpotifyClient client = SpotifyClient.create(currentUser.getRefreshToken(), yamlConfig);
    UserAuth userAuth = client.getUserAuth();
    accountService.updateTokens(userAuth);
    PlaylistManager manager = new PlaylistManager(client);
    List<Playlist> oldChosenPlaylists = playlistService.getUserPlaylists(userAuth);
    // for a new user
    if (oldChosenPlaylists.isEmpty()
        || !client.isUserActive(accountService.getTargetPlaylistSid(userAuth))) {
      String targetPlaylistSid = manager.createTargetPlaylist();
      accountService.addTargetPlaylist(userAuth, targetPlaylistSid);
      playlistService.updatePlaylists(playlists, userAuth, oldChosenPlaylists);
      Set<TrackObject> allTracksFromChosenPlaylists = manager.getAllTracksFromPlaylists(playlists);
      manager.initTargetPlaylistUpdate(allTracksFromChosenPlaylists, targetPlaylistSid);
      trackService.updateTracks(allTracksFromChosenPlaylists, userAuth);
    }
    // for active user
    else {
      playlistService.updatePlaylists(playlists, userAuth, oldChosenPlaylists);
      // filtering playlists to fetch tracks only from the new ones
      List<PlaylistObject> newPlaylists =
          manager.filterNewChosenPlaylists(oldChosenPlaylists, playlists);
      Set<TrackObject> allTracksFromChosenPlaylists =
          manager.getAllTracksFromPlaylists(newPlaylists);
      trackService.updateTracks(allTracksFromChosenPlaylists, userAuth);
    }
  }

  @GetMapping("/update")
  void update(@RequestParam("code") String code) throws Exception {
    logger.info("Start of update method");
    if (code.equals(yamlConfig.getUpdSecret())) {
      List<Account> allAccounts = accountService.getAllAccounts();
      // check updates for every user
      for (Account account : allAccounts) {
        UserAuth userAuth = null;
        logger.info("Started working with user {}", account.getUserSid());
        try {
          SpotifyClient client = SpotifyClient.create(account.getRefreshToken(), yamlConfig);
          userAuth = client.getUserAuth();
          // update tokens in db
          accountService.updateTokens(userAuth);
          // get target playlist id
          String targetPlaylistSid = account.getTargetPlaylistSid();
          // check if user is still active
          if (!client.isUserActive(targetPlaylistSid)) {
            if (targetPlaylistSid != null && !targetPlaylistSid.isEmpty()) {
              accountService.removeTargetPlaylist(userAuth);
            }
            continue;
          }
          PlaylistManager manager = new PlaylistManager(client);
          // get updated playlists
          List<Playlist> dbPlaylists = playlistService.getUserPlaylists(userAuth);
          List<PlaylistObject> updatedPlaylists = manager.getUpdatedPlaylists(dbPlaylists);
          // get new tracks
          Set<TrackObject> newTracks =
              manager.getNewTracks(updatedPlaylists, trackService.getUserTracks(userAuth));
          // if there are new tracks, put them into the playlist and save to db
          logger.info("Prepared tracks for user {} ", account.getUserSid());
          if (!newTracks.isEmpty()) {
            manager.updateTargetPlaylist(targetPlaylistSid, newTracks);
            trackService.updateTracks(newTracks, userAuth);
            // update snapshots
            playlistService.updateSnapshots(dbPlaylists, updatedPlaylists);
            logger.info("Updated playlist for user {} ", account.getUserSid());
          }
        } catch (Exception e) {
          logger.error(
              "Failed to update user {} :",
              Optional.ofNullable(userAuth).map(u -> u.getUserSid()).orElse("<unknown>"),
              e);
        }
        logger.info("Finished working with user {} ", account.getUserSid());
      }
    }
      logger.info("End of update method");
  }

  @GetMapping("/upd")
  void myUpd(@RequestParam("code") String code) throws Exception {
    if (code.equals(yamlConfig.getUpdSecret())) {
      List<Account> allAccounts = accountService.getAllAccounts();
      Account account =
          allAccounts.stream()
              .filter(acc -> acc.getUserSid().equals("11125919734"))
              .findFirst()
              .get();
      try {
        SpotifyClient client = SpotifyClient.create(account.getRefreshToken(), yamlConfig);
        UserAuth userAuth = client.getUserAuth();
        // update tokens in db
        accountService.updateTokens(userAuth);
        // get target playlist id
        String targetPlaylistSid = account.getTargetPlaylistSid();
        PlaylistManager manager = new PlaylistManager(client);
        // get updated playlists
        List<Playlist> dbPlaylists = playlistService.getUserPlaylists(userAuth);
        List<PlaylistObject> updatedPlaylists = manager.getUpdatedPlaylists(dbPlaylists);
        // get new tracks
        Set<TrackObject> newTracks =
            manager.getNewTracks(updatedPlaylists, trackService.getUserTracks(userAuth));
        // if there are new tracks, put them into the playlist and save to db
        if (!newTracks.isEmpty()) {
          manager.updateTargetPlaylist(targetPlaylistSid, newTracks);
          trackService.updateTracks(newTracks, userAuth);
          // update snapshots
          playlistService.updateSnapshots(dbPlaylists, updatedPlaylists);
        }
      } catch (Exception e) {
        logger.error("Failed to update user 11125919734", e);
      }
    }
  }
}
