package com.vseravno.playlistmanager.api;

import com.vseravno.playlistmanager.YAMLConfig;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.credentials.AuthorizationCodeCredentials;
import org.apache.hc.core5.http.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class AuthorizationCode {

  public static SpotifyApi auth(String code, YAMLConfig yamlConfig) {
    try {
      SpotifyApi spotifyApi =
          new SpotifyApi.Builder()
              .setClientId(yamlConfig.getClientId())
              .setClientSecret(yamlConfig.getClientSecret())
              .setRedirectUri(yamlConfig.getRedirectUri())
              .build();
      final AuthorizationCodeCredentials authorizationCodeCredentials =
          spotifyApi.authorizationCode(code).build().execute();
      // Set access and refresh tokens
      spotifyApi.setAccessToken(authorizationCodeCredentials.getAccessToken());
      spotifyApi.setRefreshToken(authorizationCodeCredentials.getRefreshToken());
      return spotifyApi;
    } catch (IOException | SpotifyWebApiException | ParseException e) {
      throw new RuntimeException(e);
    }
  }
}
