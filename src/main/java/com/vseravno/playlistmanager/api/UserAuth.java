package com.vseravno.playlistmanager.api;

public class UserAuth {
  private final String userSid;
  private final String accessToken;
  private final String refreshToken;

  public UserAuth(String userSid, String accessToken, String refreshToken) {
    this.userSid = userSid;
    this.accessToken = accessToken;
    this.refreshToken = refreshToken;
  }

  public String getUserSid() {
    return userSid;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public String getRefreshToken() {
    return refreshToken;
  }
}
