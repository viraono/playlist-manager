package com.vseravno.playlistmanager.api;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


public class AuthRequest {
    private String authCode;

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getAuthCode() {
        return authCode;
    }
    @JsonCreator
    public AuthRequest(@JsonProperty("code") String authCode) {
        this.authCode = authCode;
    }

    //json needs empty constructor
    public AuthRequest() {}
}
