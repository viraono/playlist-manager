package com.vseravno.playlistmanager.db;

import com.google.common.collect.Sets;
import com.vseravno.playlistmanager.PlaylistObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vseravno.playlistmanager.api.UserAuth;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class PlaylistService {

  private final PlaylistRepository playlistRepository;

  private final AccountService accountService;

  @Autowired
  public PlaylistService(PlaylistRepository playlistRepository, AccountService accountService) {
    this.playlistRepository = playlistRepository;
    this.accountService = accountService;
  }

  public void updatePlaylists(
      List<PlaylistObject> chosenPlaylists, UserAuth auth, List<Playlist> dbPlaylists)
      throws Exception {
    Long accountId = accountService.getAccountId(auth);

    // for new user
    if (dbPlaylists.isEmpty()) {
      savePlaylists(chosenPlaylists, accountId);
    }

    // for existing user
    else {
      Set<String> dbSids =
          dbPlaylists.stream().map(Playlist::getPlaylistSid).collect(Collectors.toSet());
      Set<String> chosenPlaylistSids =
          chosenPlaylists.stream().map(PlaylistObject::playlistSid).collect(Collectors.toSet());
      // if playlist is in db but not in chosen, it's removed
      List<Playlist> removedPlaylists =
          dbPlaylists.stream()
              .filter(pl -> !chosenPlaylistSids.contains(pl.getPlaylistSid()))
              .collect(Collectors.toList());
      // if playlist is in chosen, but not in db, it's new
      List<PlaylistObject> newPlaylists =
          chosenPlaylists.stream()
              .filter(pl -> !dbSids.contains(pl.playlistSid()))
              .collect(Collectors.toList());

      removePlaylists(removedPlaylists);
      savePlaylists(newPlaylists, accountId);
    }
  }

  private void savePlaylists(List<PlaylistObject> chosenPlaylists, long accountId) {
    for (PlaylistObject playlistObject : chosenPlaylists) {
      Playlist playlist = new Playlist();
      playlist.setAccountId(accountId);
      playlist.setPlaylistSid(playlistObject.playlistSid());
      playlist.setPlaylistSnapshot(playlistObject.playlistSnapshot());
      playlist.setInserted(Timestamp.from(Instant.now()));
      playlistRepository.save(playlist);
    }
  }

  private void removePlaylists(List<Playlist> removedPlaylists) {
    for (Playlist pl : removedPlaylists) {
      playlistRepository.delete(pl);
    }
  }

  public void updateSnapshots(List<Playlist> dbPlaylists, List<PlaylistObject> playlistsToUpdate) {

    // map for new snapshots
    Map<String, String> sidsAndNewSnapshots =
        playlistsToUpdate.stream()
            .collect(
                Collectors.toMap(PlaylistObject::playlistSid, PlaylistObject::playlistSnapshot));

    for (Playlist pl : dbPlaylists) {
      String playlistSid = pl.getPlaylistSid();
      // find playlist by id
      if (sidsAndNewSnapshots.containsKey(playlistSid)) {
        String newSnapshot = sidsAndNewSnapshots.get(playlistSid);
        // updating snapshot
        pl.setPlaylistSnapshot(newSnapshot);
        playlistRepository.save(pl);
      }
    }
  }

  public List<Playlist> getUserPlaylists(UserAuth auth){
    String userSid = auth.getUserSid();
    return playlistRepository.findAllUserPlaylistsByUserSid(userSid);
  }

  public Map<String, Long> getPlaylistsSidsAndIds(UserAuth auth) throws Exception {
    Map<String, Long> idsAndSids = new HashMap<>();
    List<Playlist> userPlaylists = getUserPlaylists(auth);
    for (Playlist pl : userPlaylists) {
      idsAndSids.put(pl.getPlaylistSid(), pl.getId());
    }
    return idsAndSids;
  }
}
