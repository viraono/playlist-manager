package com.vseravno.playlistmanager.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.vseravno.playlistmanager.api.UserAuth;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
public class AccountService {

  private final AccountRepository accountRepository;

  @Autowired
  public AccountService(AccountRepository accountRepository) {
    this.accountRepository = accountRepository;
  }

  // get all accounts
  public List<Account> getAllAccounts() {
    return accountRepository.findAll();
  }

  public boolean userExist(UserAuth auth) {
    return accountRepository.findOptionalAccountBySpotifyId(auth.getUserSid()).isPresent();
  }



  // get user's refresh token by Spotify id
  public String getRefreshTokenByUserSid(UserAuth auth) throws Exception {
    return accountRepository
        .findRefreshTokenBySpotifyId(auth.getUserSid())
        .orElseThrow(
            () -> new Exception("Refresh token from user with id " + auth.getUserSid() + " not found"));
  }

  public Account getUserByAccessToken(String accessToken)throws Exception{
    return accountRepository
            .findAccountByAccessToken(accessToken)
            .orElseThrow(
                    () -> new Exception("User with accessToken " + accessToken + " not found"));
  }

  // create an account
  public void createAccount(UserAuth auth) {
    Account account = new Account();
    account.setUserSid(auth.getUserSid());
    account.setAccessToken(auth.getAccessToken());
    account.setRefreshToken(auth.getRefreshToken());
    account.setInserted(Timestamp.from(Instant.now()));
    accountRepository.save(account);
  }

  public void addTargetPlaylist(UserAuth auth, String targetPlaylistSid){
    Account account = accountRepository.findAccountBySpotifyId(auth.getUserSid());
    account.setTargetPlaylistSid(targetPlaylistSid);
    accountRepository.save(account);
  }

  public void removeTargetPlaylist(UserAuth auth){
    Account account = accountRepository.findAccountBySpotifyId(auth.getUserSid());
    account.setTargetPlaylistSid("");
    accountRepository.save(account);
  }

  public String getTargetPlaylistSid(UserAuth auth){
    Account account =
        accountRepository
            .findAccountBySpotifyId(auth.getUserSid());
    return account.getTargetPlaylistSid();
  }

  // update access_token
  public void updateTokens(UserAuth auth) throws Exception {
    Account account =
        accountRepository
            .findAccountBySpotifyId(auth.getUserSid());

    account.setRefreshToken(auth.getRefreshToken());
    account.setAccessToken(auth.getAccessToken());
    accountRepository.save(account);
  }

  public Long getAccountId(UserAuth auth) throws Exception {
    return accountRepository
        .findAccountIdByUserSpotifyId(auth.getUserSid());
  }

  public boolean deleteAccount(UserAuth auth) throws Exception {
    Account account = accountRepository
        .findAccountBySpotifyId(auth.getUserSid());
    accountRepository.delete(account);
    return true;
  }
}
