package com.vseravno.playlistmanager.db;

import com.google.common.base.Objects;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "track")
public class Track {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @Column(name="account_id")
  private long accountId;

  @Column(name="track_sid")
  private String trackSid;

  @Column(name="playlist_id")
  private Long playlistId;

  private java.sql.Timestamp inserted;

  public Track() {}

  public Track(long accountId, String trackSid, Timestamp inserted) {
    this.accountId = accountId;
    this.trackSid = trackSid;
    this.inserted = inserted;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getPlaylistId() {
    return playlistId;
  }

  public void setPlaylistId(long playlistId) {
    this.playlistId = playlistId;
  }

  public long getAccountId() {
    return accountId;
  }

  public void setAccountId(long accountId) {
    this.accountId = accountId;
  }

  public String getTrackSid() {
    return trackSid;
  }

  public void setTrackSid(String trackSid) {
    this.trackSid = trackSid;
  }

  public Timestamp getInserted() {
    return inserted;
  }

  public void setInserted(Timestamp inserted) {
    this.inserted = inserted;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Track track = (Track) o;
    return id == track.id &&
        accountId == track.accountId &&
        playlistId == track.playlistId &&
        Objects.equal(trackSid, track.trackSid);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id, accountId, trackSid, playlistId);
  }

  @Override
  public String toString() {
    return "Track{" +
        "id=" + id +
        ", accountId=" + accountId +
        ", trackSid='" + trackSid + '\'' +
        ", playlistId=" + playlistId +
        '}';
  }
}
