package com.vseravno.playlistmanager.db;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

    @Query("SELECT a.refreshToken FROM Account a where a.userSid = :userSid")
    Optional<String> findRefreshTokenBySpotifyId(@Param("userSid") String userSpotifyId);

    @Query("SELECT a FROM Account a where a.userSid = :userSid")
    Optional<Account> findOptionalAccountBySpotifyId(@Param("userSid") String userSpotifyId);

    @Query("SELECT a FROM Account a where a.userSid = :userSid")
    Account findAccountBySpotifyId(@Param("userSid") String userSpotifyId);

    @Query("SELECT a.id FROM Account a where a.userSid = :userSid")
    Long findAccountIdByUserSpotifyId(@Param("userSid") String userSpotifyId);

    @Query("SELECT a FROM Account a where a.accessToken = :accessToken")
    Optional<Account> findAccountByAccessToken(@Param("accessToken") String accessToken);

}
