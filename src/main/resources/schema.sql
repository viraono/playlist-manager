CREATE TABLE account (
id serial PRIMARY KEY,
user_sid TEXT NOT NULL,
access_token TEXT NOT NULL,
refresh_token TEXT NOT NULL,
target_playlist_sid TEXT,
inserted TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now());

CREATE TABLE playlist (
id serial PRIMARY KEY,
account_id INTEGER REFERENCES account (id) NOT NULL,
playlist_sid TEXT NOT NULL,
playlist_snapshot TEXT NOT NULL,
inserted TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now());

CREATE TABLE track (
id serial PRIMARY KEY,
account_id INTEGER REFERENCES account (id) NOT NULL,
playlist_id INTEGER REFERENCES playlist (id) ON DELETE SET NULL,
track_sid TEXT NOT NULL,
inserted TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now());