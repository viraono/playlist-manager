package com.vseravno.playlistmanager.api;

import com.google.common.collect.Lists;
import com.google.common.io.Resources;
import com.squareup.okhttp.mockwebserver.MockResponse;
import com.squareup.okhttp.mockwebserver.MockWebServer;
import com.squareup.okhttp.mockwebserver.RecordedRequest;
import com.vseravno.playlistmanager.TrackObject;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.model_objects.miscellaneous.PlaylistTracksInformation;
import com.wrapper.spotify.model_objects.specification.PlaylistSimplified;
import com.wrapper.spotify.model_objects.specification.PlaylistTrack;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static net.javacrumbs.jsonunit.assertj.JsonAssertions.assertThatJson;

public class SpotifyClientTest {
  MockWebServer spotifyApiMock;
  SpotifyClient client;

  private String getJson(String resourceName) {
    try {
      return Resources.toString(
          Resources.getResource("jsonForTests/" + resourceName + ".json"), StandardCharsets.UTF_8);
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException();
    }
  }

  @AfterEach
  void tearDown() throws IOException {
    spotifyApiMock.shutdown();
  }

  @BeforeEach
  void initialize() throws InterruptedException, IOException {
    spotifyApiMock = new MockWebServer();
    spotifyApiMock.start();

    spotifyApiMock.enqueue(
        new MockResponse()
            .addHeader("Content-Type", "application/json")
            .setBody(getJson("testUserObject")));

    client =
        SpotifyClient.create(
            new SpotifyApi.Builder()
                .setHost(spotifyApiMock.getHostName())
                .setPort(spotifyApiMock.getPort())
                .setAccessToken("accessToken")
                .setScheme("http")
                .build());

    spotifyApiMock.takeRequest();
  }

  @Test
  public void getUserPlaylistsLessThanLimit() throws Exception {
    spotifyApiMock.enqueue(
        new MockResponse()
            .addHeader("Content-Type", "application/json")
            .setBody(getJson("testUserPlaylists10items")));

    List<PlaylistSimplified> playlists = client.getUserPlaylists();

    assertEquals(10, playlists.size());
  }

  @Test
  public void getUserPlaylistsMoreThanLimit() throws Exception {
    spotifyApiMock.enqueue(
        new MockResponse()
            .addHeader("Content-Type", "application/json")
            .setBody(getJson("testUserPlaylists50items0offset")));

    spotifyApiMock.enqueue(
        new MockResponse()
            .addHeader("Content-Type", "application/json")
            .setBody(getJson("testUserPlaylists50items50offset")));

    List<PlaylistSimplified> playlists = client.getUserPlaylists();

    assertEquals(100, playlists.size());
  }

  @Test
  public void getAllTracksFromPlaylists() throws InterruptedException {
    spotifyApiMock.enqueue(
        new MockResponse()
            .addHeader("Content-Type", "application/json")
            .setBody(getJson("testPlaylistTracks100items0offset")));
    spotifyApiMock.enqueue(
        new MockResponse()
            .addHeader("Content-Type", "application/json")
            .setBody(getJson("testPlaylistTracks100items100offset")));
    spotifyApiMock.enqueue(
        new MockResponse()
            .addHeader("Content-Type", "application/json")
            .setBody(getJson("testPlaylistTracks10items")));

    List<PlaylistSimplified> playlists = new ArrayList<>();
    playlists.add(
        new PlaylistSimplified.Builder()
            .setId("1")
            .setTracks(new PlaylistTracksInformation.Builder().setTotal(200).build())
            .build());
    playlists.add(
        new PlaylistSimplified.Builder()
            .setId("2")
            .setTracks(new PlaylistTracksInformation.Builder().setTotal(10).build())
            .build());

    Set<TrackObject> tracks = client.getAllTracksFromPlaylists(playlists);

    assertEquals(210, tracks.size());
  }

  @Test
  public void addTrackstoPlaylist() throws InterruptedException {
    spotifyApiMock.enqueue(
        new MockResponse()
            .addHeader("Content-Type", "application/json")
            .setBody(getJson("testPlaylistTracks100items0offset")));

    spotifyApiMock.enqueue(new MockResponse().addHeader("Content-Type", "application/json"));

    // preparing list with TrackObjects
    List<PlaylistSimplified> playlists = new ArrayList<>();
    playlists.add(
        new PlaylistSimplified.Builder()
            .setId("1")
            .setTracks(new PlaylistTracksInformation.Builder().setTotal(100).build())
            .build());
    Set<TrackObject> tracks = client.getAllTracksFromPlaylists(playlists);

    client.addTracksToPlaylist("id", Lists.newArrayList(tracks));

    spotifyApiMock.takeRequest();
    RecordedRequest request = spotifyApiMock.takeRequest();

    assertThatJson(getJson("trackUris")).isEqualTo(request.getBody().readUtf8());
  }

  @Test
  public void getMaxAmountOfTracksOnce() {
    spotifyApiMock.enqueue(
        new MockResponse()
            .addHeader("Content-Type", "application/json")
            .setBody(getJson("testPlaylistTracks100items100offset")));

    PlaylistSimplified playlist =
        new PlaylistSimplified.Builder()
            .setId("1")
            .setTracks(new PlaylistTracksInformation.Builder().setTotal(200).build())
            .build();

    List<PlaylistTrack> tracks = client.getMaxAmountOfTracksOnce(playlist, 100);
    assertEquals(100, tracks.size());
  }
}
